package store

import (
	"testing"

	"github.com/go-test/deep"
)

var comments = []*Comment{
	{
		ID:         1417659383947779410,
		parentID:   0,
		PostSlug:   "go+is+bad+because+it+has+no+generics",
		AuthorName: "diamondburned",
		Content:    "ok retard",
		PP:         1,
	},
	{
		ID:         1535083312847589647,
		parentID:   1417659383947779410,
		PostSlug:   "go+is+bad+because+it+has+no+generics",
		AuthorName: "ym555",
		Content:    "no u",
		PP:         1,
	},
	{
		ID:         1408319441107161354,
		parentID:   1535083312847589647,
		PostSlug:   "go+is+bad+because+it+has+no+generics",
		AuthorName: "diamondburned",
		Content:    "retard.",
		PP:         1,
	},
	{
		ID:         1385494439993796678,
		parentID:   0,
		PostSlug:   "go+is+bad+because+it+has+no+generics",
		AuthorName: "lidl_papi",
		Content:    "use rust",
		PP:         1,
	},
	{
		ID:         1313938001973060307,
		parentID:   1385494439993796678,
		PostSlug:   "go+is+bad+because+it+has+no+generics",
		AuthorName: "diamondburned",
		Content:    "ok retard",
		PP:         1,
	},
}

var nestedComments = []*Comment{
	{
		ID:         1417659383947779410,
		parentID:   0,
		PostSlug:   "go+is+bad+because+it+has+no+generics",
		AuthorName: "diamondburned",
		Content:    "ok retard",
		PP:         1,
		Children: []*Comment{
			{
				ID:         1535083312847589647,
				parentID:   1417659383947779410,
				PostSlug:   "go+is+bad+because+it+has+no+generics",
				AuthorName: "ym555",
				Content:    "no u",
				PP:         1,
				Children: []*Comment{
					{
						ID:         1408319441107161354,
						parentID:   1535083312847589647,
						PostSlug:   "go+is+bad+because+it+has+no+generics",
						AuthorName: "diamondburned",
						Content:    "retard.",
						PP:         1,
					},
				},
			},
		},
	},
	{
		ID:         1385494439993796678,
		parentID:   0,
		PostSlug:   "go+is+bad+because+it+has+no+generics",
		AuthorName: "lidl_papi",
		Content:    "use rust",
		PP:         1,
		Children: []*Comment{
			{
				ID:         1313938001973060307,
				parentID:   1385494439993796678,
				PostSlug:   "go+is+bad+because+it+has+no+generics",
				AuthorName: "diamondburned",
				Content:    "ok retard",
				PP:         1,
			},
		},
	},
}

func testNestedComments(t *testing.T) {
	original := make([]*Comment, len(comments))
	copy(original, comments)

	if diff := deep.Equal(nestComments(original), nestedComments); diff != nil {
		t.Fatal(diff)
	}
}

func testComment(t *testing.T) {
	testNestedComments(t)

	for _, c := range comments {
		if err := InsertComment(c.parentID, c); err != nil {
			t.Fatal(err)
		}
	}

	c, err := QueryCommentFromPost("go+is+bad+because+it+has+no+generics")
	if err != nil {
		t.Fatal(err)
	}

	if diff := deep.Equal(nestedComments, nestComments(c)); diff != nil {
		t.Error(diff)
	}

	c, err = QueryCommentChildren("1385494439993796678")
	if err != nil {
		t.Fatal(err)
	}

	if diff := deep.Equal(
		[]*Comment{nestedComments[1]},
		nestComments(c),
	); diff != nil {
		t.Error(diff)
	}
}

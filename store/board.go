package store

import "time"

// BoardsSchema is the schema for the boards table
const BoardsSchema = `
CREATE TABLE IF NOT EXISTS boards (
    name          VARCHAR(64) PRIMARY KEY,
    banner_url    TEXT,
    creation_date DATETIME,
    hidden        BOOLEAN
)
`

// Board is the 1:1 mapping from the SQL table
// schema
type Board struct {
	Name         string
	BannerURL    string
	CreationDate time.Time
	Hidden       bool
}

func InsertBoard(b *Board, username string) error {
	_, err := db.Exec(
		`INSERT INTO boards VALUES (?, ?, ?, ?)`,
		b.Name, b.BannerURL, b.CreationDate, b.Hidden,
	)

	if err != nil {
		return err
	}

	return InsertBoardStaff(&BoardStaff{
		Username:   username,
		Board:      b.Name,
		Permission: PermissionOwner,
	})
}

func QueryBoard(name string) (*Board, error) {
	r := db.QueryRow(`SELECT * FROM boards WHERE name = ?`, name)
	b := &Board{}

	return b, r.Scan(
		&b.Name, &b.BannerURL, &b.CreationDate, &b.Hidden,
	)
}

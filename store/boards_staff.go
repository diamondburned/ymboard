package store

import "database/sql"

// BoardStaffSchema is the schema for the staff
// table. This relates to the boards table.
const BoardStaffSchema = `
CREATE TABLE IF NOT EXISTS board_staff (
    username   VARCHAR(255),
    board      VARCHAR(64),
    permission ENUM("owner", "mod"),
  
    FOREIGN KEY (username)
        REFERENCES users(username)
        ON DELETE CASCADE,
    FOREIGN KEY (board)
        REFERENCES boards(name)
        ON DELETE CASCADE
)
`

// BoardStaff is the 1:1 mapping from the SQL
// table schema
type BoardStaff struct {
	Username   string
	Board      string
	Permission BoardStaffPermission
}

// BoardStaffPermission is the 1:1 mapping from
// the `permission` field, type ENUM()
type BoardStaffPermission string

const (
	// PermissionNone is nothing
	PermissionNone BoardStaffPermission = ""

	// PermissionOwner indicates the owner, or
	// creator, of the board
	PermissionOwner BoardStaffPermission = "owner"

	// PermissionMod indicates the moderator
	// or the board. Optional.
	PermissionMod BoardStaffPermission = "mod"
)

// InsertBoardStaff inserts a board staff
func InsertBoardStaff(bs *BoardStaff) error {
	_, err := db.Exec(
		"INSERT INTO board_staff VALUES (?, ?, ?)",
		bs.Username, bs.Board, bs.Permission,
	)

	return err
}

func GetPermissionFromBoard(username, board string) (BoardStaffPermission, error) {
	q := db.QueryRow(
		`SELECT permission FROM board_staff
			WHERE username = ? AND board = ?`,
		username, board,
	)

	var p = PermissionNone
	err := q.Scan(&p)

	if err == sql.ErrNoRows {
		return PermissionNone, nil
	}

	return p, err
}

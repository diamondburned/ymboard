package store

import (
	"sort"
	"time"
)

// PostsSchema is the schema for all posts.
const PostsSchema = `
CREATE TABLE IF NOT EXISTS posts (
    post_slug     VARCHAR(128) PRIMARY KEY,
  	title         VARCHAR(255) UNIQUE,
    author_name   VARCHAR(255),
    time          DATETIME,
  	board         VARCHAR(64),
  	pp            BIGINT(255),
  	content       LONGTEXT,
  	post_type     ENUM("text", "image", "link") NOT NULL,

	FOREIGN KEY (author_name)
		REFERENCES users(username)
		ON DELETE CASCADE,
	FOREIGN KEY (board)
		REFERENCES boards(name)
		ON DELETE CASCADE
)
`

// Post is the 1:1 mapping from the SQL table
// schema
type Post struct {
	PostSlug   string
	Title      string
	AuthorName string
	Time       time.Time
	Board      string
	PP         int64
	Content    string
	PostType   PostType
}

// PostType is the 1:1 mapping from the table's
// post_type field, type ENUM()
type PostType string

const (
	// TextPost indicates a post with formatted
	// rich text as its content
	TextPost PostType = "text"

	// ImagePost indicates a post linked to
	// an image
	ImagePost PostType = "image"

	// LinkPost indicates a post linked to
	// another URL
	LinkPost PostType = "link"
)

// InsertPost inserts to a post
func InsertPost(p *Post) error {
	_, err := db.Exec(
		`INSERT INTO posts VALUES (?, ?, ?, ?, ?, ?, ?, ?);`,
		p.PostSlug, p.Title, p.AuthorName, p.Time,
		p.Board, p.PP, p.Content, p.PostType,
	)

	return err
}

// QueryPostsInBoard queries all the posts in a board, with
// `limit`, sorted by pp and time by default
func QueryPostsInBoard(name string, limit, page int) ([]*Post, error) {
	if page < 0 {
		page = 0
	}

	offset := limit * page

	r, err := db.Query(
		`SELECT * FROM posts WHERE board = ? 
			ORDER BY time DESC LIMIT ?, ?`,
		name, offset, limit,
	)

	if err != nil {
		return nil, err
	}

	defer r.Close()

	var posts = make([]*Post, 0, limit)

	for r.Next() {
		var p = &Post{}

		if err := r.Scan(
			&p.PostSlug, &p.Title, &p.AuthorName, &p.Time,
			&p.Board, &p.PP, &p.Content, &p.PostType,
		); err != nil {
			return nil, err
		}

		posts = append(posts, p)
	}

	sort.Slice(posts, func(i, j int) bool {
		return posts[i].PP > posts[j].PP
	})

	return posts, nil
}

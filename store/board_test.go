package store

import (
	"testing"

	"github.com/go-test/deep"
)

var board = &Board{
	Name:         "reddit",
	BannerURL:    "",
	CreationDate: mustSQLDate("2016-07-25 01:12:34"),
	Hidden:       false,
}

func testBoard(t *testing.T) {
	if err := InsertBoard(board, "diamondburned"); err != nil {
		t.Fatal(err)
	}

	b, err := QueryBoard("reddit")
	if err != nil {
		t.Fatal(err)
	}

	if diff := deep.Equal(b, board); diff != nil {
		t.Error(diff)
	}

	testBoardStaff(t)
}

func testBoardStaff(t *testing.T) {
	p, err := GetPermissionFromBoard("diamondburned", "reddit")
	if err != nil {
		t.Fatal(err)
	}

	if p != PermissionOwner {
		t.Fatal("diamond is not owner, test failed")
	}
}

func testBoardFollower(t *testing.T) {
	if err := InsertBoardFollower(&BoardFollower{
		Username: "diamondburned",
		Board:    "reddit",
	}); err != nil {
		t.Fatal(err)
	}
}

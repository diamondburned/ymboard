package store

import (
	"time"
)

// UsersSchema is the schema for the users table
const UsersSchema = `
CREATE TABLE IF NOT EXISTS users (
    username      VARCHAR(255) PRIMARY KEY,
    joined_date   DATETIME,
    email         VARCHAR(255) UNIQUE,
    password_hash BINARY(60),
    avatar_url    TEXT,
    pp            BIGINT(255)
)
`

// User is the 1:1 mapping from the SQL table
// schema
type User struct {
	Username     string
	JoinedDate   time.Time
	Email        string
	PasswordHash []byte
	AvatarURL    string
	PP           int64
}

/* TODO:
   - Add methods to modify email, password and avatar
   - Add methods to increment PP
*/

// InsertUser inserts a new user
func InsertUser(u *User) error {
	_, err := db.Exec(
		`INSERT INTO users VALUES (?, ?, ?, ?, ?, ?)`,
		u.Username, u.JoinedDate, u.Email,
		u.PasswordHash, u.AvatarURL, u.PP,
	)

	return err
}

// IncrementPP increments the PP by 1
func IncrementPP(username string) error {
	_, err := db.Exec(
		`UPDATE users SET pp = pp + 1 WHERE username = ?`,
		username,
	)

	return err
}

// QueryUser queries an user
func QueryUser(username string) (*User, error) {
	// SELECT * FROM users WHERE username = "ym555"
	r := db.QueryRow("SELECT * FROM users WHERE username = ?", username)
	u := &User{}

	return u, r.Scan(
		&u.Username, &u.JoinedDate, &u.Email,
		&u.PasswordHash, &u.AvatarURL, &u.PP,
	)
}

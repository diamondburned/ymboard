package store

// BoardsFollowersSchema is the schema for the
// followers. This relates to the boards table.
const BoardFollowerSchema = `
CREATE TABLE IF NOT EXISTS board_followers (
    username   VARCHAR(255),
    board      VARCHAR(64),
  
    FOREIGN KEY (username)
        REFERENCES users(username)
        ON DELETE CASCADE,
    FOREIGN KEY (board)
        REFERENCES boards(name)
        ON DELETE CASCADE
)
`

// BoardFollower is the 1:1 mapping from the SQL
// table schema
type BoardFollower struct {
	Username string
	Board    string
}

func InsertBoardFollower(bf *BoardFollower) error {
	_, err := db.Exec(
		"INSERT INTO board_followers VALUES (?, ?)",
		bf.Username, bf.Board,
	)

	return err
}

func GetBoardFollower(username string) ([]*BoardFollower, error) {
	q, err := db.Query(
		"SELECT * FROM boards_followers WHERE username = ?",
		username,
	)

	if err != nil {
		return nil, err
	}

	defer q.Close()

	var fs []*BoardFollower

	for q.Next() {
		var f = &BoardFollower{}

		if err := q.Scan(&f.Username, &f.Board); err != nil {
			return nil, err
		}

		fs = append(fs, f)
	}

	return fs, nil
}

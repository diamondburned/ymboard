package store

import (
	"sort"
	"testing"

	"github.com/go-test/deep"
)

var posts = []*Post{
	{
		PostSlug:   "go+is+bad+because+it+has+no+generics",
		Title:      "Go is bad because it has no generics!!!",
		AuthorName: "ym555",
		Time:       mustSQLDate("2016-07-27 05:32:19"),
		Board:      "reddit",
		PP:         3,
		Content:    "lol Go bad no generics, rust is great haha 8=D",
		PostType:   TextPost,
	},
	{
		PostSlug:   "announcing+ymboard+for+dumb+people",
		Title:      "Announcing ymboard for dumb people",
		AuthorName: "diamondburned",
		Time:       mustSQLDate("2016-07-25 02:53:12"),
		Board:      "reddit",
		PP:         5,
		Content:    "https://cdn.discordapp.com/avatars/257954736689512448/f925bb25741bdc1f9938c88b539ecdaf.png",
		PostType:   ImagePost,
	},
	{
		PostSlug:   "6cord+stable83+is+released",
		Title:      "6cord stable-8.3 is released",
		AuthorName: "diamondburned",
		Time:       mustSQLDate("2019-05-24 17:03:12"),
		Board:      "reddit",
		PP:         1,
		Content:    "https://gitlab.com/diamondburned/6cord",
		PostType:   LinkPost,
	},
}

func testPosts(t *testing.T) {
	for _, p := range posts {
		if err := InsertPost(p); err != nil {
			t.Fatal(err)
		}
	}

	sort.Slice(posts, func(i, j int) bool {
		return posts[i].PP > posts[j].PP
	})

	// No need to sort for date, as pp sorting in SQL should
	// take first priority anyway

	p, err := QueryPostsInBoard("reddit", 3, 0)
	if err != nil {
		t.Fatal(err)
	}

	if diff := deep.Equal(p, posts); diff != nil {
		t.Error(diff)
	}
}

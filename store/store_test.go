package store

import (
	"strings"
	"testing"
	"time"
)

const initExec = "SET FOREIGN_KEY_CHECKS=0;" +
	"DROP TABLE IF EXISTS `boards`, `boards_followers`, `boards_staff`, `comments`, `posts`, `users`;" +
	"SET FOREIGN_KEY_CHECKS=1;"

func TestSchema(t *testing.T) {
	if err := Initialize("root", "", "test_ymboard"); err != nil {
		t.Fatal(err)
	}

	defer db.Close()

	for _, schema := range strings.Split(initExec, ";") {
		if schema == "" {
			continue
		}

		if _, err := db.Exec(schema); err != nil {
			t.Fatal(err)
		}
	}

	for _, schema := range schemas {
		if _, err := db.Exec(schema); err != nil {
			t.Fatal(err)
		}
	}

	// testUsers inserts all users and asserts
	// them
	testUsers(t)

	// testBoard inserts a board and asserts
	// it
	testBoard(t)

	// testPosts inserts posts and asserts
	// them
	testPosts(t)

	// testComment test the nesting function,
	// inserts comments, then asserts them
	testComment(t)
}

func mustSQLDate(s string) time.Time {
	t, err := time.Parse(sqlTime, s)
	if err != nil {
		panic(err)
	}

	return t
}

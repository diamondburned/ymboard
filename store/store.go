// Package store provides functions to query the SQL
// database for raw structures
package store

const sqlTime = `2006-01-02 15:04:05`

var schemas = [...]string{
	UsersSchema,
	BoardsSchema,
	BoardStaffSchema,
	BoardFollowerSchema,
	PostsSchema,
	CommentsSchema,
}

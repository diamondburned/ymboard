package store

import (
	"database/sql"
)

// CommentsSchema is the schema for all comments,
// stored in a flat table with nested IDs.
const CommentsSchema = `
CREATE TABLE IF NOT EXISTS comments (
	id          BIGINT(255) NOT NULL PRIMARY KEY,
  	parent_id   BIGINT(255) NULL,
  	post_slug   VARCHAR(128),
  	author_name VARCHAR(255),
  	content     LONGTEXT,
  	pp          BIGINT(255),
  
    FOREIGN KEY (parent_id) 
        REFERENCES comments(id),
    FOREIGN KEY (post_slug)
        REFERENCES posts(post_slug)
        ON DELETE CASCADE,
    FOREIGN KEY (author_name)
        REFERENCES users(username)
        ON DELETE CASCADE
)
`

// Comment is the 1:1 mapping from the SQL table
// schema
type Comment struct {
	ID         int64 // Unixtime
	PostSlug   string
	AuthorName string
	Content    string
	PP         int64
	Children   []*Comment

	parentID int64
}

// InsertComment inserts a comment
func InsertComment(parentID int64, c *Comment) error {
	pID := &sql.NullInt64{
		Valid: parentID != 0,
		Int64: parentID,
	}

	_, err := db.Exec(
		`INSERT INTO comments VALUES (?, ?, ?, ?, ?, ?)`,
		c.ID, pID, c.PostSlug,
		c.AuthorName, c.Content, c.PP,
	)

	return err
}

// QueryCommentFromPost queries all comments with the post slug
func QueryCommentFromPost(slug string) ([]*Comment, error) {
	r, err := db.Query("SELECT * FROM comments WHERE post_slug = ?", slug)
	if err != nil {
		return nil, err
	}

	defer r.Close()

	return parseCommentQuery(r)
}

func QueryCommentChildren(id string) ([]*Comment, error) {
	r, err := db.Query(`
		WITH recursive cte (id, parent_id, post_slug, author_name, content, pp) AS (
			SELECT id, parent_id, post_slug, author_name, content, pp FROM comments 
				WHERE id = ?
			UNION ALL
			SELECT c.id, c.parent_id, c.post_slug, c.author_name, c.content, c.pp
				FROM comments c
			JOIN cte ON c.parent_id = cte.id
		) SELECT * FROM cte;
	`, id)

	if err != nil {
		return nil, err
	}

	defer r.Close()

	return parseCommentQuery(r)
}

package store

import (
	"database/sql"
	"fmt"

	// MySQL driver
	_ "github.com/go-sql-driver/mysql"
)

var (
	db   *sql.DB
	stmt = map[string]*sql.Stmt{}
)

// Initialize initializes the database
func Initialize(username, password, database string) error {
	if db != nil {
		if db.Ping() == nil {
			return nil
		}

		db.Close()
	}

	s, err := sql.Open("mysql", fmt.Sprintf(
		"%s:%s@/%s?parseTime=true",
		username, password, database,
	))

	if err != nil {
		return err
	}

	db = s

	for _, schema := range schemas {
		if _, err := db.Exec(schema); err != nil {
			db.Close()
			return err
		}
	}

	return nil
}

// Close closes the store and its resources
func Close() error {
	for _, s := range stmt {
		if err := s.Close(); err != nil {
			return err
		}
	}

	if db != nil {
		return db.Close()
	}

	return nil
}

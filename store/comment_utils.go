package store

import (
	"database/sql"
	"sort"
)

func parseCommentQuery(r *sql.Rows) ([]*Comment, error) {
	var comments = make([]*Comment, 0, 10)

	for r.Next() {
		var (
			c   = &Comment{}
			pID = &sql.NullInt64{}
		)

		if err := r.Scan(
			&c.ID, pID, &c.PostSlug, &c.AuthorName,
			&c.Content, &c.PP,
		); err != nil {
			return nil, err
		}

		c.parentID = pID.Int64

		comments = append(comments, c)
	}

	return nestComments(comments), nil
}

func nestComments(input []*Comment) (ret []*Comment) {
	sort.Slice(input, func(i, j int) bool {
		return input[i].ID > input[j].ID
	})

	sort.SliceStable(input, func(i, j int) bool {
		return input[i].PP > input[j].PP
	})

Main:
	for _, c := range input {
		for _, d := range input {
			if d.ID == c.parentID {
				d.Children = append(d.Children, c)
				continue Main
			}
		}

		ret = append(ret, c)
	}

	return
}

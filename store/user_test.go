package store

import (
	"testing"
	"time"

	"github.com/go-test/deep"
	"golang.org/x/crypto/bcrypt"
)

/*
INSERT INTO users VALUES (
	"diamondburned",
	"2016-07-24 13:34:10",
	"email1@domain.com",
	"hash",
	"https://cdn.discordapp.com/avatars/170132746042081280/5a8a9f925f78304ac3e9d313f836cf26.png",
	8
);

INSERT INTO users VALUES (
	"ym555",
	"2016-07-24 13:34:53",
	"email2@domain.com",
	"hash",
	"https://cdn.discordapp.com/avatars/170132746042081280/5a8a9f925f78304ac3e9d313f836cf26.png",
	4
);

INSERT INTO users VALUES (
	"lidl_papi",
	"2016-07-24 13:35:12",
	"email3@domain.com",
	"hash",
	"https://cdn.discordapp.com/avatars/170132746042081280/5a8a9f925f78304ac3e9d313f836cf26.png",
	1
);
*/

// password is password
var password = []byte{
	0x24, 0x32, 0x61, 0x24, 0x31, 0x30, 0x24, 0x77, 0x64, 0x70, 0x7a, 0x44, 0x57, 0x43, 0x41, 0x78,
	0x52, 0x6d, 0x32, 0x46, 0x31, 0x55, 0x43, 0x48, 0x70, 0x73, 0x7a, 0x74, 0x4f, 0x4b, 0x6b, 0x45,
	0x54, 0x4e, 0x4c, 0x2f, 0x61, 0x35, 0x74, 0x66, 0x2f, 0x63, 0x72, 0x56, 0x73, 0x4e, 0x64, 0x31,
	0x41, 0x2f, 0x46, 0x34, 0x7a, 0x76, 0x7a, 0x77, 0x70, 0x6b, 0x72, 0x75,
}

var users = map[string]*User{
	"diamondburned": {
		Username:     "diamondburned",
		JoinedDate:   mustDate("Sun Jul 24 13:34:10 2016"),
		Email:        "email1@domain.com",
		PasswordHash: password,
		AvatarURL:    "https://cdn.discordapp.com/avatars/170132746042081280/5a8a9f925f78304ac3e9d313f836cf26.png",
		PP:           8,
	},
	"ym555": {
		Username:     "ym555",
		JoinedDate:   mustDate("Sun Jul 24 13:34:53 2016"),
		Email:        "email2@domain.com",
		PasswordHash: password,
		AvatarURL:    "https://cdn.discordapp.com/avatars/170132746042081280/5a8a9f925f78304ac3e9d313f836cf26.png",
		PP:           4,
	},
	"lidl_papi": {
		Username:     "lidl_papi",
		JoinedDate:   mustDate("Sun Jul 24 13:35:12 2016"),
		Email:        "email3@domain.com",
		PasswordHash: password,
		AvatarURL:    "https://cdn.discordapp.com/avatars/170132746042081280/5a8a9f925f78304ac3e9d313f836cf26.png",
		PP:           1,
	},
}

func testUsers(t *testing.T) {
	for _, u := range users {
		n := &(*u)
		n.PP = 0

		if err := InsertUser(n); err != nil {
			t.Fatal(err)
		}

		for i := 0; i < int(u.PP); i++ {
			if err := IncrementPP(u.Username); err != nil {
				t.Fatal(err)
			}
		}
	}

	var newUsers = make(map[string]*User, 3)

	for _, u := range users {
		u, err := QueryUser(u.Username)
		if err != nil {
			t.Fatal(err)
		}

		newUsers[u.Username] = u
	}

	if diff := deep.Equal(newUsers, users); diff != nil {
		t.Error(diff)
	}

	if err := bcrypt.CompareHashAndPassword(
		newUsers["diamondburned"].PasswordHash,
		[]byte("password"),
	); err != nil {
		t.Error(err)
	}
}

func mustDate(s string) time.Time {
	t, err := time.Parse(time.ANSIC, s)
	if err != nil {
		panic(err)
	}

	return t
}

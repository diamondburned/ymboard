module github.com/diamondburned/ymboard

go 1.12

require (
	github.com/alexedwards/scs v1.4.1
	github.com/alexedwards/scs/v2 v2.0.0
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/go-sql-driver/mysql v1.4.1
	github.com/go-test/deep v1.0.1
	github.com/gorilla/schema v1.1.0
	github.com/k0kubun/pp v3.0.1+incompatible
	github.com/mattn/go-colorable v0.1.2 // indirect
	golang.org/x/crypto v0.0.0-20190530122614-20be4c3c3ed5
)

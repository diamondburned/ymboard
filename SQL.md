# SQL

## Slugging

```go
title := "6cord stable-8.3 is released"
slug := strings.Map(func(r rune) rune {
	switch {
	case unicode.IsLower(r) || unicode.IsDigit(r):
		return r
	case unicode.IsUpper(r):
	    return unicode.ToLower(r)
	case r == ' ':
		return '+'
	}

	return -1
}, title)

fmt.Println(slug)
```

## Schema

```sql
CREATE TABLE IF NOT EXISTS users (
    username      VARCHAR(255) PRIMARY KEY,
    joined_date   DATETIME,
    email         VARCHAR(255) UNIQUE,
    password_hash CHAR(60),
    avatar_url    TEXT,
    pp            BIGINT(255)
);

CREATE TABLE IF NOT EXISTS boards (
    name          VARCHAR(64) PRIMARY KEY,
    banner_url    TEXT,
    creation_date DATETIME,
    hidden        BOOLEAN
);

CREATE TABLE IF NOT EXISTS boards_staff (
    username   VARCHAR(255),
    board      VARCHAR(64),
    permission ENUM("owner", "mod"),
  
    FOREIGN KEY (username)
        REFERENCES users(username)
        ON DELETE CASCADE,
    FOREIGN KEY (board)
        REFERENCES boards(name)
        ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS boards_followers (
    username   VARCHAR(255),
    board      VARCHAR(64),
  
    FOREIGN KEY (username)
        REFERENCES users(username)
        ON DELETE CASCADE,
    FOREIGN KEY (board)
        REFERENCES boards(name)
        ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS posts (
    post_slug     VARCHAR(128) PRIMARY KEY,
  	title         VARCHAR(255) UNIQUE,
    author_name   VARCHAR(255),
    time          DATETIME,
  	board         VARCHAR(64),
  	pp            BIGINT(255),
  	content       LONGTEXT,
  	post_type     ENUM("text", "image", "link") NOT NULL,

	FOREIGN KEY (author_name)
		REFERENCES users(username)
		ON DELETE CASCADE,
	FOREIGN KEY (board)
		REFERENCES boards(name)
		ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS comments (
	id          BIGINT(255) NOT NULL PRIMARY KEY,
  	parent_id   BIGINT(254) NULL,
  	post_slug   VARCHAR(128),
  	author_name VARCHAR(255),
  	content     LONGTEXT,
  	pp          BIGINT(255),
  
    FOREIGN KEY (parent_id) 
        REFERENCES comments(id),
    FOREIGN KEY (post_slug)
        REFERENCES posts(post_slug)
        ON DELETE CASCADE,
    FOREIGN KEY (author_name)
        REFERENCES users(username)
        ON DELETE CASCADE
);
```

## Test Data

```sql
INSERT INTO users VALUES (
	"diamondburned",
	"2016-07-24 13:34:10",
	"email1@domain.com",
	"hash",
	"https://cdn.discordapp.com/avatars/170132746042081280/5a8a9f925f78304ac3e9d313f836cf26.png",
	8
);

INSERT INTO users VALUES (
	"ym555",
	"2016-07-24 13:34:53",
	"email2@domain.com",
	"hash",
	"https://cdn.discordapp.com/avatars/170132746042081280/5a8a9f925f78304ac3e9d313f836cf26.png",
	4
);

INSERT INTO users VALUES (
	"lidl_papi",
	"2016-07-24 13:35:12",
	"email3@domain.com",
	"hash",
	"https://cdn.discordapp.com/avatars/170132746042081280/5a8a9f925f78304ac3e9d313f836cf26.png",
	1
);

INSERT INTO boards VALUES (
	"reddit",
	"",
	"2016-07-25 01:12:34",
	0
);

INSERT INTO boards_staff VALUES (
	"diamondburned",
	"reddit",
	"owner"
);

INSERT INTO boards_followers VALUES (
	"diamondburned",
	"reddit"
);

INSERT INTO posts VALUES (
	"go+is+bad+because+it+has+no+generics",
	"Go is bad because it has no generics!!!",
	"ym555",
	"2016-07-27 05:32:19",
	"reddit",
	3,
	"lol Go bad no generics, rust is great haha 8=D",
	"text"
);

INSERT INTO posts VALUES (
	"announcing+ymboard+for+dumb+people",
	"Announcing ymboard for dumb people",
	"diamondburned",
	"2016-07-25 02:53:12",
	"reddit",
	5,
	"https://cdn.discordapp.com/avatars/257954736689512448/f925bb25741bdc1f9938c88b539ecdaf.png",
	"image"
);

INSERT INTO posts VALUES (
	"6cord+stable83+is+released",
	"6cord stable-8.3 is released",
	"diamondburned",
	"2019-05-24 17:03:12",
	"reddit",
	1,
	"https://gitlab.com/diamondburned/6cord",
	"link"
);

INSERT INTO comments VALUES (
	"1417659383947779410",
	NULL,
	"go+is+bad+because+it+has+no+generics",
	"diamondburned",
	"ok retard",
	1
);

INSERT INTO comments VALUES (
	"1535083312847589647",
	"1417659383947779410",
	"go+is+bad+because+it+has+no+generics",
	"ym555",
	"no u",
	1
);

INSERT INTO comments VALUES (
	"1408319441107161354",
	"1535083312847589647",
	"go+is+bad+because+it+has+no+generics",
	"diamondburned",
	"retard.",
	1
);

INSERT INTO comments VALUES (
	"1385494439993796678",
	NULL,
	"go+is+bad+because+it+has+no+generics",
	"lidl_papi",
	"use rust",
	1
);

INSERT INTO comments VALUES (
	"1313938001973060307",
	"1385494439993796678",
	"go+is+bad+because+it+has+no+generics",
	"diamondburned",
	"ok retard",
	1
);

```

## Queries

### Getting an user's information

```sql
SELECT * FROM users WHERE username = "ym555";
SELECT * FROM posts WHERE author_name = "ym555";
SELECT * FROM comments WHERE author_name = "ym555";
```

### Getting all comments in a post

```sql
SELECT * FROM comments WHERE post_slug = "go+is+bad+because+it+has+no+generics";
```

### Recursively get children for a comment

```sql
WITH recursive cte (id, parent_id, author_name, content, pp) AS (
	SELECT id, parent_id, author_name, content, pp FROM comments 
	WHERE id = "1417659383947779410"

	UNION ALL

	SELECT c.id, c.parent_id, c.author_name, c.content, c.pp FROM comments c 
	JOIN cte ON c.parent_id = cte.id
)
SELECT * FROM cte;
```

### Getting all posts in a subboard

```sql
SELECT * FROM posts WHERE subboard = "weirdstuff";
```



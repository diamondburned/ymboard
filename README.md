# ymboard

```sql
CREATE TABLE IF NOT EXISTS users (
    username      TINYTEXT,
    joined_date   DATETIME,
    email         TEXT(65535),
    password_hash CHAR(60),
    avatar_url    TEXT(65535),
    pp            BIGINT(255)
);

INSERT INTO users VALUES (
	"diamondburned",
  	"2019-05-25",
	"sql@asd",
  	"asdaaaceeecfe",
    "https://cdn.discordapp.com/avatars/257954736689512448/f925bb25741bdc1f9938c88b539ecdaf.png",
  	2314141334
);
```



package routes

import (
	"github.com/go-chi/chi"
	"github.com/gorilla/schema"
)

var formDec = schema.NewDecoder()

// Router is the main router
func Router(r chi.Router) {
	r.Use(session.LoadAndSave)

	r.Route("/auth", func(r chi.Router) {
		r.Route("/login", func(r chi.Router) {
			r.With(checkAuthMiddleware).Get("/", renderLoginPage)
			r.With(parseFormMiddleware).Post("/", loginHandler)
		})

		r.Route("/register", func(r chi.Router) {
			r.Get("/", renderRegisterPage)
			r.With(parseFormMiddleware).Post("/", registerHandler)
		})

		r.Delete("/logout", logoutHandler)
	})

	r.Route("/b", func(r chi.Router) {
		r.Get("/", ListAllBoards)

		r.Route("/{board}", func(r chi.Router) {
			r.Use(parseFormMiddleware)

			r.Get("/", ListAllPostsInBoard)
			r.Post("/", PostInBoard)
		})
	})
}

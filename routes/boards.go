package routes

import (
	"net/http"

	"github.com/diamondburned/ymboard/store"
	"github.com/go-chi/chi"
)

func ListAllBoards(w http.ResponseWriter, r *http.Request) {}

type ListAllPostsInBoardForm struct {
	Page int `schema:"page"`
}

func ListAllPostsInBoard(w http.ResponseWriter, r *http.Request) {
	var form ListAllPostsInBoardForm
	if err := formDec.Decode(&form, r.PostForm); err != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	p, err := store.QueryPostsInBoard(chi.URLParam(r, "board"), 25, form.Page)
	if err != nil {
		http.Error(w, err.Error(), 404)
		return
	}

	_ = p
}

func PostInBoard(w http.ResponseWriter, r *http.Request) {

}

package routes

import (
	"net/http"

	"github.com/diamondburned/ymboard/store"
	"golang.org/x/crypto/bcrypt"
)

type authLogin struct {
	Username string `schema:"username,required"`
	Password string `schema:"password,required"`
}

func loginHandler(w http.ResponseWriter, r *http.Request) {
	var a authLogin

	if err := formDec.Decode(&a, r.PostForm); err != nil {
		loginError(w, r, err)
		return
	}

	u, err := store.QueryUser(a.Username)
	if err != nil {
		loginError(w, r, err)
		return
	}

	if err := bcrypt.CompareHashAndPassword(
		u.PasswordHash, []byte(a.Password),
	); err != nil {
		loginError(w, r, err)
		return
	}
}

// This function refreshes the page with the token in the cookies. The token
// stores the login error. When the login page loads, it pops the error out
// and render it.
func loginError(w http.ResponseWriter, r *http.Request, err error) {
	session.Put(r.Context(), sessionLoginErrorKey, err.Error())
	http.Redirect(w, r, "", 400) // should refresh
}

func renderLoginPage(w http.ResponseWriter, r *http.Request) {
	_ = session.GetString(r.Context(), sessionLoginErrorKey)
}

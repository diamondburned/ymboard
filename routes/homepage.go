package routes

import "net/http"

func renderHomePage(w http.ResponseWriter, r *http.Request) {
	// use this in templating
	_ = session.PopString(r.Context(), sessionErrorKey)
}

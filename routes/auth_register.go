package routes

import (
	"errors"
	"net/http"
	"regexp"
	"time"

	"github.com/diamondburned/ymboard/store"
	"golang.org/x/crypto/bcrypt"
)

type authRegister struct {
	authLogin
	Email     string `schema:"email,required"`
	AvatarURL string `schema:"avatar_url"`
}

var (
	// https://github.com/badoux/checkmail/blob/master/checkmail.go#L37
	emailRegexp = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")

	errInvalidEmail = errors.New("Invalid email")
)

func registerHandler(w http.ResponseWriter, r *http.Request) {
	var a authRegister

	// Decode form into struct
	if err := formDec.Decode(&a, r.PostForm); err != nil {
		registerError(w, r, err)
		return
	}

	// Check if valid email
	if !emailRegexp.MatchString(a.Email) {
		registerError(w, r, errInvalidEmail)
		return
	}

	// Hash password
	h, err := bcrypt.GenerateFromPassword([]byte(a.Password), 10) // default cost
	if err != nil {
		registerError(w, r, err)
		return
	}

	// User struct
	u := &store.User{
		Username:     a.Username,
		JoinedDate:   time.Now(),
		Email:        a.Email,
		PasswordHash: h,
		AvatarURL:    a.AvatarURL,
		PP:           0,
	}

	// Store the user into db
	if err := store.InsertUser(u); err != nil {
		registerError(w, r, err)
		return
	}

	// Store the user in session cache
	session.Put(r.Context(), sessionUserKey, u)

	// Redirect back to homepage
	http.Redirect(w, r, "/", http.StatusOK)
}

func registerError(w http.ResponseWriter, r *http.Request, err error) {
	session.Put(r.Context(), sessionRegisterErrorKey, err.Error())
	http.Redirect(w, r, "", http.StatusBadRequest)
}

// vv calls vv

func renderRegisterPage(w http.ResponseWriter, r *http.Request) {
	_ = session.PopString(r.Context(), sessionRegisterErrorKey)
}

package routes

import (
	"errors"
	"time"

	"github.com/alexedwards/scs/v2"
)

type ymSession struct {
	*scs.Session
}

var session = newSession()

const (
	sessionErrorKey         = "error"
	sessionUserKey          = "user"
	sessionLoginErrorKey    = "login_error"
	sessionRegisterErrorKey = "register_error"
)

var errUnauthenticated = errors.New("user is unauthenticated")

func newSession() *ymSession {
	ses := scs.NewSession()
	ses.Lifetime = 24 * time.Hour

	return &ymSession{
		Session: ses,
	}
}

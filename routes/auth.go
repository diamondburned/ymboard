package routes

import (
	"net/http"

	"github.com/diamondburned/ymboard/store"
)

// Done
func checkAuthMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if err := r.ParseForm(); err != nil {
			http.Error(w, err.Error(), 400)
			return
		}

		_, ok := session.Get(r.Context(), sessionUserKey).(*store.User)
		if ok {
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}

		next.ServeHTTP(w, r)
	})
}

// Done
func logoutHandler(w http.ResponseWriter, r *http.Request) {
	// Delete the session
	session.Remove(r.Context(), sessionUserKey)

	// Redirect to the homepage
	http.Redirect(w, r, "/", http.StatusFound)
}
